FROM python:3.9-slim

WORKDIR /app
COPY . /app

RUN pip install --no-cache-dir Flask
RUN pip install --upgrade pip
RUN apt-get update && apt-get install make
RUN pip install Flask

EXPOSE 5000

ENV FLASK_APP=app.py
ENV FLASK_RUN_HOST=0.0.0.0

CMD ["flask", "run"]
